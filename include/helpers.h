#ifndef HELPERS_H
#define HELPERS_H
#include <memory>

using namespace std;

// Some convenience enums
enum class Status {fail, success};

enum class Direction {from, to, both};

enum class TraversalMethod {
    // DEPTH first
    inorder_asc,    // left subtree, node, right subtree
    inorder_desc,   // right subtree, node, left subtree
    preorder,       // node, left subtree, right subtree
    postorder,      // left subtree, right subtree, node

    // BREADTH first
    breadth_left,         // breadth-first, left-first
    breadth_right,        // breadth-first, right-first

    // DEFAULT
    none            // no explicit search method specified. For flagging purposes.
};

// display shared pointer information from shared_ptr (with reference count)
template<typename T>
void disp(const std::shared_ptr<T>& o, const char* printName = "disp")
{
    if(o)
    {
        cout << printName << ": " << o.get()->value();
        cout << " (" << o.use_count() << ")";
        cout << " mem: " << &o;
        cout << " ptr: " << o.get();
        cout << endl;
    }
    else
        cout << printName << " [null]" << endl;
}

// display shared pointer information from shared_ptr (with reference count)
template<typename T>
void disp(const std::unique_ptr<T>& o, const char* printName)
{
    if(o)
    {
        cout << printName << ": " << o.get()->value();
        cout << " mem: " << &o;
        cout << " ptr: " << o.get();
        cout << endl;
    }
    else
        cout << printName << " [null]" << endl;
}

// print STL container
// works for vector, list, deque, set, multiset, unordered_set, unordered_multiset
template<template<class...> class T, class S>
void printSTLContainer(const T<S>& container)
{
    std::cout << "{ ";
    for(const S& elem : container)
    {
        std::cout << elem << " ";
    }
    std::cout << "}" << std::endl;
}

// works with map, multimap, unordered_map, unordered_multimap
template<template<class...> class T, class S, class U>
void printSTLMap(const T<S,U>& container)
{
    std::cout << "{ ";
    for(const auto& elem : container)
    {
        std::cout << elem.first << ":" << elem.second << ", ";
    }
    std::cout << "}" << std::endl;
}

// works with map, multimap, unordered_map, unordered_multimap
template<template<class...> class T, class S, template<class...> class Vec, class Type>
void printSTLMapOfVectors(const T<S, Vec<Type>>& container) // e.g. map<int, vector<float>>
{
    std::cout << "{ " << std::endl;
    for(const auto& elem : container)
    {
        std::cout << elem.first << ": ";

        for(const auto& e : elem.second)
            std::cout << e << ", ";

        std::cout << std::endl;
    }
    std::cout << "}" << std::endl;
}

#endif // HELPERS_H
