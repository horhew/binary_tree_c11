#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "binary_tree.h"
#include "helpers.h"

using namespace std;

int main()
{
    cout << "############### INT ###############" << endl;

    // initialize
    cout << "Initializing BinaryTree<int> bti(12)" << endl;
    BinaryTree<int> bti(12);
    cout << endl;

    // insert by single value
    cout << "insert(const int&): inserting 5 and 15." << endl;
    bti.insert(5).insert(15);
    cout << endl;

    // insert by vector of values
    cout << "insert(const vector<int>): inserting multiple values." << endl;
    vector<int> v1 {3,7,13,20};
    vector<int> v2 {2,4,6,8};
    vector<int> v3 {1,10};
    vector<int> v4 {9,11};
    bti.insert(v1).insert(v2).insert(v3).insert(v4);
    cout << endl;

    // pretty print
    cout << "pprint(): pretty printing tree bti:" << endl;
    bti.pprint();
    cout << endl;

    // copy assignment
    cout << "operator=(BinaryTree&): copy assignment operator." << endl;
    BinaryTree<int> bti1;
    bti1 = bti;
    cout << "bti1: "; bti1.print();
    cout << endl;

    // copy constructor
    cout << "BinaryTree(BinaryTree&): copy constructor." << endl;
    BinaryTree<int> bti2(bti);
    cout << "bti2: "; bti2.print();
    cout << endl;

    // search
    cout << "search(const int&): search bti2 for the value 12:" << endl;
    auto a = bti2.search(12);
    disp(a.lock(), "bti2.search(12)");
    cout << endl;

    // set traversal method (used in print methods)
    cout << "setTraversalMethod(const TraversalMethod&): breadth_left." << endl;
    bti1.setTraversalMethod(TraversalMethod::breadth_left);
    cout << endl;

    // print
    cout << "print(): print tree bti1:" << endl;
    bti1.print();
    cout << endl;

    // remove
    cout << "remove(const int&): remove 2, 1, 11, 10, 5, 12 from tree bti:" << endl;
    bti.remove(2); // one child
    bti.remove(1); // no children

    bti.remove(11); // no children
    bti.remove(10); // one child

    bti.remove(5); // two children
    bti.remove(12); // two children (root)
    cout << "bti: "; bti.pprint();
    cout << endl;

    // find min, max
    cout << "findMin() & findMax(): find min and max values in tree bti:" << endl;
    auto mn = bti.findMin();
    auto mx = bti.findMax();
    if(mn.lock() && mx.lock())
    {
        cout << "MIN: " << mn.lock()->value() << endl;
        cout << "MAX: " << mx.lock()->value() << endl;
    }
    cout << endl;

    // print to vector
    cout << "printToVec(vector<int>&): output values of bti1 to a vector in desc order:" << endl;
    vector<int> out_int;
    bti.printToVec(out_int, TraversalMethod::inorder_desc);
    printSTLContainer(out_int);
    cout << "bti size: " << out_int.size() << endl;
    cout << endl;

    // destroy (fail)
    cout << "destroy(<start_node>): failed destruction of bti starting at <start_node>:" << endl;
    auto c = bti.search(152).lock();
    bti.destroy(c);
    cout << endl;

    // destroy (success)
    cout << "destroy(<start_node>): destruction of subtree of bti2, starting at 7:" << endl;
    auto d = bti2.search(7).lock();
    bti2.destroy(d);
    bti2.pprint();

    cout << "destroy(): destruction of bti:" << endl;
    bti.destroy();
    cout << endl;

    // try to print an expired pointer
    if(!mx.expired())
        cout << mx.lock()->value() << endl;

    // insert a value in a previously destroyed tree
    cout << "insert(const int&): insert a value (204) in a previously destroyed tree." << endl;
    bti.insert(204);
    cout << endl;

    // stats
    cout << "stats ----------------------" << endl;
    cout << "Num nodes bti : " << bti.numNodes() << endl;
    cout << "Num nodes bti1: " << bti1.numNodes() << endl;
    cout << "Num nodes bti2: " << bti2.numNodes() << endl;

    cout << "bti : "; bti.print();
    cout << "bti1: "; bti1.print();
    cout << "bti2: "; bti2.print();
    cout << endl;


    // ----------------- string -----------------
    cout << endl << "############### STRING ###############" << endl;

    // initialize
    cout << "Initializing BinaryTree<string>." << endl;
    BinaryTree<string> bts;
    bts.setCaseSensitive(false);
    cout << endl;

    // insert by single value
    cout << "insert(const string&): inserting \"gamma\", \"delta\", \"tau\"" << endl;
    bts.insert("gamma").insert("delta").insert("tau");
    cout << endl;

    // insert by vector of values
    cout << "insert(const vector<string>): inserting multiple values." << endl;
    vector<string> w {"Alpha", "epsilon", "Theta", "phi", "Rho"};
    bts.insert(w);
    cout << endl;

    // pretty print
    cout << "pprint(): pretty printing tree bts:" << endl;
    bts.pprint();
    cout << endl;

    // copy assignment
    cout << "operator=(BinaryTree&): copy assignment operator." << endl;
    BinaryTree<string> bts1;
    bts1 = bts;
    cout << "bts1: "; bts1.print();
    cout << endl;

    // copy constructor
    cout << "BinaryTree(BinaryTree&): copy constructor." << endl;
    BinaryTree<string> bts2(bts);
    cout << "bts2: "; bts2.print();
    cout << endl;

    // search
    cout << "search(const int&): search bts2 for the value \"gamma\":" << endl;
    auto s = bts2.search("gamma");
    disp(s.lock());
    cout << endl;

    // set traversal method (used in print methods)
    cout << "setTraversalMethod(const TraversalMethod&): inorder_desc." << endl;
    bts1.setTraversalMethod(TraversalMethod::inorder_desc);
    cout << endl;

    // print
    cout << "print(): print tree bts1:" << endl;
    bts1.print();
    cout << endl;

    // remove
    cout << "remove(const string&): remove \"phi\",\"theta\",\"delta\" from tree bts:" << endl;
    bts.remove("phi"); // one child
    bts.remove("theta"); // no children
    bts.remove("delta"); // two children
    cout << "bts: "; bts.pprint();
    cout << endl;

    // print to vector
    cout << "printToVec(vector<string>&): output values of bts1 to a vector in desc order:" << endl;
    vector<string> out_string;
    bts1.printToVec(out_string, TraversalMethod::inorder_desc);
    printSTLContainer(out_string);
    cout << "bts1 size: " << out_string.size() << endl;
    cout << endl;

    // destroy (fail)
    cout << "destroy(<start_node>): failed destruction of bts starting at <start_node>:" << endl;
    auto t = bts.search("george").lock();
    bts.destroy(t);
    cout << endl;

    // destroy (success)
    cout << "destroy(<start_node>): destruction of subtree of bts2, starting at \"tau\":" << endl;
    auto u = bts2.search("tau").lock();
    bts2.destroy(u);
    bts2.pprint();

    cout << "destroy(): destruction of bts:" << endl;
    bts.destroy();
    cout << endl;

    // insert a value in a previously destroyed tree
    cout << "insert(const string&): insert a value (\"george\") in a previously destroyed tree." << endl;
    bts.insert("george");
    cout << endl;

    // stats
    cout << "stats ----------------------" << endl;
    cout << "Num nodes bts : " << bts.numNodes() << endl;
    cout << "Num nodes bts1: " << bts1.numNodes() << endl;
    cout << "Num nodes bts2: " << bts2.numNodes() << endl;

    cout << "bts : "; bts.print();
    cout << "bts1: "; bts1.print();
    cout << "bts2: "; bts2.print();
    cout << endl;

    return 0;
}
