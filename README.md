## Binary Tree (C++ 11/14/17)

A no-frills template-driven binary tree using C++ 11 / 14 / 17 features. Went a bit overkill with the templates and usage of smart pointers, but the reason behind writing it was precisely to exercise those things.
*Note: Project was created with Qt Creator, hence the inclusion of the .pro file, but there are no actual Qt dependencies.*
**_Needs C++ 17 compiler to build_**... obviously.

***

#### Some features

- Only two main classes at work: **BinaryTree\<T\>** and **Node\<T\>**.

- The tree is doubly-linked, meaning each Node holds 2 **shared_ptr**'s to its children and a **weak_ptr** to its parent (to avoid circular dependency issues).

- The usual insert(), remove(), findMin(), findMax() etc. methods are implemented, as well as a (deep) copy constructor and copy assignment operator.

- The tree supports both depth-first (4 modes) and breadth-first (2 modes) traversal. Because of a multitude of methods needing to traverse the tree, a design decision was made to have a single traverse() method that accepts a function pointer to the function performing the actual action on the nodes (e.g. print, remove, copy).

Can't think of anything else interesting to mention, like I said, this was just an exercise with a bit of beyond-the-basics templates and modern C++ features. If anything, it can serve as demonstration of some template syntax for later reference.

#### External libraries

None