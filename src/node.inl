#include "node.h"

template<class T>
Node<T>& Node<T>::operator=(const Node<T>& other)
{
    if(this != &other) // self assignment check
    {
        m_value = other.value();
        m_left = other.left();
        m_right = other.right();
        m_parent = other.parent();
    }

    return *this;
}

template<class T>
void Node<T>::destroy()
{
    // deep destroy as necessary (in case there is any dynamic memory to deallocate)
}

template<class T>
void Node<T>::setParent(const shared_ptr<Node<T>>& parent)
{
    if(parent == nullptr)
        m_parent.reset();
    else
        m_parent = parent;
}

template<class T>
void Node<T>::setChild(const shared_ptr<Node<T>>& child)
{
    (child->value() < m_value) ? m_left = child : m_right = child;
}

template<class T>
void Node<T>::unparent(const Direction& direction)
{
    // remove link FROM & TO parent
    if(m_parent.lock() && direction == Direction::both)
    {
        if(_isLargerThanParent())
            m_parent.lock()->setRight(nullptr);
        else
            m_parent.lock()->setLeft(nullptr);

        m_parent.lock() = nullptr;
    }
    // remove link FROM parent
    else if(m_parent.lock() && direction == Direction::from)
    {
        if(_isLargerThanParent())
            m_parent.lock()->setRight(nullptr);
        else
            m_parent.lock()->setLeft(nullptr);
    }
    // remove link TO parent
    else
    {
        m_parent.lock() = nullptr;
    }
}

template<class T>
bool Node<T>::_isLargerThanParent() const
{
    if(m_parent.lock())
        return (m_value > m_parent.lock()->value()) ? true : false;
    else
        return false;
}

template<class T>
bool Node<T>::isLarger(const shared_ptr<Node<T>>& other) const
{
    if(!other)
        throw "Comparison node is a nullptr.";

    return (m_value > other->value()) ? true : false;
}
