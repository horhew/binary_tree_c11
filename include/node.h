#ifndef NODE_H
#define NODE_H

#include <algorithm>
#include <iostream>
#include <map>
#include <memory>

#include "helpers.h"

using namespace std;

template<class T>
class Node : public enable_shared_from_this<Node<T>>
{
private:
    const T             m_value;
    shared_ptr<Node<T>> m_left; // TODO: try unique_ptr
    shared_ptr<Node<T>> m_right; // TODO: try unique_ptr
    weak_ptr<Node<T>>   m_parent;

    bool _isLargerThanParent() const;
public:
    Node() = delete;
    // TODO: Add copy constructor
    Node(const T& val, const shared_ptr<Node<T>>& parent = nullptr) : m_value(val), m_parent(parent) {} // explicit ?
    ~Node() { /*cout << "dtor(" << m_value << ")" << endl;*/ }

    shared_ptr<Node<T>>& left() { return m_left; }
    weak_ptr<Node<T>>    parent() { return m_parent; }
    shared_ptr<Node<T>>& right() { return m_right; }
    const T&             value() const { return m_value; }
    Node<T>& operator=(const Node<T>& other);

    void destroy();
    bool isLarger(const shared_ptr<Node<T>>& other) const;
    void setChild(const shared_ptr<Node<T>>& child);
    void setLeft(const shared_ptr<Node<T>>& left) { m_left = left; }
    void setParent(const shared_ptr<Node<T>>& parent = nullptr);
    void setRight(const shared_ptr<Node<T>>& right) { m_right = right; }
    void unparent(const Direction& direction = Direction::both);
};

#include "node.inl"

#endif // NODE_H
