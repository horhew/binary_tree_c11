#include "binary_tree.h"

template<class T>
BinaryTree<T>::BinaryTree(BinaryTree& other)
{
    // test for empty other tree
    if(other.numNodes() != 0)
    {
        // copy root
        m_root = make_shared<Node<T>>(other.root().lock()->value());
        m_numNodes++;

        // copy all other nodes
        auto otherRoot = other.root().lock();
        _copyNode(otherRoot, m_root);
    }
}


template<class T>
BinaryTree<T>& BinaryTree<T>::operator=(BinaryTree& other)
{
    if(this != &other) // self assignment check
    {
        // destroy existing tree
        destroy();

        // test for empty other tree
        if(other.numNodes() != 0)
        {
            // copy root
            m_root = make_shared<Node<T>>(other.root().lock()->value());
            m_numNodes++;

            // copy all other nodes
            auto otherRoot = other.root().lock();
            _copyNode(otherRoot, m_root);

        }

        return *this;
    }

    return *this;
}

template<class T>
void BinaryTree<T>::_copyNode(shared_ptr<Node<T>>& otherNode, shared_ptr<Node<T>>& newNode)
{
    if(otherNode->left())
    {
        // create left child
        auto left = make_shared<Node<T>>(otherNode->left()->value(), newNode);
        newNode->setLeft(left);
        m_numNodes++;

        // keep moving to the left
        _copyNode(otherNode->left(), newNode->left());
    }

    if(otherNode->right())
    {
        // create right child
        auto right = make_shared<Node<T>>(otherNode->right()->value(), newNode);
        newNode->setRight(right);
        m_numNodes++;

        // keep moving to the right
        _copyNode(otherNode->right(), newNode->right());
    }
}

template<class T>
void BinaryTree<T>::_destroyNode(shared_ptr<Node<T>>& node, const TraversalMethod& method)
{
    // method needed just to conform to the signature traverse() expects
    if(method == TraversalMethod::inorder_asc) {}

    node->unparent();
    node.reset();
    m_numNodes--;
}

template<class T>
void BinaryTree<T>::destroy()
{
    if(m_root)
    {
        auto fn = &BinaryTree<T>::_destroyNode;
        traverse(fn, m_root, TraversalMethod::postorder);
        cout << "Tree destroyed." << endl;
    }
}

template<class T>
void BinaryTree<T>::destroy(shared_ptr<Node<T>>& startNode)
{
    string msg;

    if(startNode)
    {
        auto startValue = startNode->value();
        auto fn = &BinaryTree<T>::_destroyNode;
        traverse(fn, startNode, TraversalMethod::postorder);
        cout << "Subtree starting at " << startValue << " destroyed.";
    }
    else
        cout << "Invalid start node. Cannot destroy tree.";

    cout << endl;
}

template<class T>
void BinaryTree<T>::print(const TraversalMethod& method)
{
    // Do some checks before proceeding with traversal
    if(!m_root) // empty tree
        cout << "Empty tree." << endl;
    else
    {
        // using Fn = void (BinaryTree<T>::*) (const shared_ptr<Node<T>>& node, <rest_of_args...>); // alternative to auto
        auto fn = &BinaryTree<T>::_printNode;
        traverse(fn, m_root, method);

        cout << endl;
    }
}

template<class T> // print
void BinaryTree<T>::printToVec(vector<T>& out, const TraversalMethod& method)
{
    // Do some checks before proceeding with traversal
    if(!m_root) // empty tree
        cout << "Empty tree." << endl;
    else
    {
        // using Fn = void (BinaryTree<T>::*) (const shared_ptr<Node<T>>& node, <rest_of_args...>); // alternative to auto
        auto fn = &BinaryTree<T>::_printNodeToVec;
        traverse(fn, m_root, method, out);
    }
}

template<class T>
void BinaryTree<T>::_printNode(const shared_ptr<Node<T>>& node, const TraversalMethod& method)
{
    if(method == TraversalMethod::breadth_left || method == TraversalMethod::breadth_right)
    {
        // queue required for breadth-first traversal
        queue<Node<T>*> que;
        que.push(node.get());

        while(que.size() > 0)
        {
            cout << que.front()->value() << ", ";

            // add the node's children (if any) to the queue
            if(method == TraversalMethod::breadth_left)
            {
                if(que.front()->left())
                    que.push(que.front()->left().get());
                if(que.front()->right())
                    que.push(que.front()->right().get());
            }
            else if(method == TraversalMethod::breadth_right)
            {
                if(que.front()->right())
                    que.push(que.front()->right().get());
                if(que.front()->left())
                    que.push(que.front()->left().get());
            }

            que.pop();
        }
    }
    else
        cout << node->value() << ", ";
}

template<class T>
void BinaryTree<T>::_printNodeToVec(const shared_ptr<Node<T>>& node,
                                    const TraversalMethod& method,
                                    vector<T>& out)
{
    if(method == TraversalMethod::breadth_left || method == TraversalMethod::breadth_right)
    {
        // queue required for breadth-first traversal
        queue<Node<T>*> que;
        que.push(node.get());

        while(que.size() > 0)
        {
            // store node's value
            out.push_back(que.front()->value());

            // add its children (if any) to the queue
            if(method == TraversalMethod::breadth_left)
            {
                if(que.front()->left())
                    que.push(que.front()->left().get());
                if(que.front()->right())
                    que.push(que.front()->right().get());
            }
            else if(method == TraversalMethod::breadth_right)
            {
                if(que.front()->right())
                    que.push(que.front()->right().get());
                if(que.front()->left())
                    que.push(que.front()->left().get());
            }

            // pop the node
            que.pop();
        }
    }
    else
        out.push_back(node->value());
}

template<class T>
template<class... Args1, class... Args2>
void BinaryTree<T>::traverse(void (BinaryTree<T>::*fn)(Args1...),
                             shared_ptr<Node<T>>& node,
                             const TraversalMethod& method,
                             Args2&... args) // multiple args of different types can be passed here
{
    if(method == TraversalMethod::none) // default to the object's private traversal method
    {
        traverse(fn, node, m_traversalMethod, args...);
    }
    else if(method == TraversalMethod::inorder_asc)
    {
        if(node->left())
            traverse(fn, node->left(), method, args...);

        (this->*fn)(node, method, args...);

        if(node->right())
            traverse(fn, node->right(), method, args...);
    }
    else if(method == TraversalMethod::inorder_desc)
    {
        if(node->right())
            traverse(fn, node->right(), method, args...);

        (this->*fn)(node, method, args...);

        if(node->left())
            traverse(fn, node->left(), method, args...);
    }
    else if(method == TraversalMethod::preorder)
    {
        (this->*fn)(node, method, args...);

        if(node->left())
            traverse(fn, node->left(), method, args...);

        if(node->right())
            traverse(fn, node->right(), method, args...);
    }
    else if(method == TraversalMethod::postorder)
    {
        if(node->left())
            traverse(fn, node->left(), method, args...);

        if(node->right())
            traverse(fn, node->right(), method, args...);

        (this->*fn)(node, method, args...);
    }
    else // breadth_left, breadth_right
    {
        cout << "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&" << endl;
        (this->*fn)(node, method, args...);
    }
}

template<class T>
BinaryTree<T>& BinaryTree<T>::_insert(const T& val, shared_ptr<Node<T>>& startNode)
{
    auto status = Status::success;

    if(!startNode) // inserting into an empty tree
    {
        m_root = make_shared<Node<T>>(val);
    }
    else if(val == startNode->value()) // found a duplicate
    {
        cout << "Value " << val << " already exists" << endl;
        status = Status::fail;
    }
    else if(val < startNode->value()) // keep going left
    {
        if(!startNode->left())
        {
            // nowhere to go left, just insert a new node here.
            auto left = make_shared<Node<T>>(val, startNode); // QTCreator 4.6.2 complains about too few args
            startNode->setLeft(left);
        }
        else // keep going left
        {
            _insert(val, startNode->left());
            status = Status::fail;
        }
    }
    else // keep going right
    {
        if(!startNode->right())
        {
            // nowhere to go right, just insert a new node here.
            auto right = make_shared<Node<T>>(val, startNode); // QTCreator 4.6.2 complains about too few args
            startNode->setRight(right);
        }
        else // keep going right
        {
            _insert(val, startNode->right());
            status = Status::fail;
        }
    }

    // increment only on successful node insertion
    m_numNodes += (status == Status::success) ? 1 : 0;

    return *this;
}

// insert by single value (generic)
template<class T>
BinaryTree<T>& BinaryTree<T>::insert(const T& val)
{
    return _insert(val, m_root);
}

// insert by single value (string template specialization)
// see https://stackoverflow.com/questions/4445654/multiple-definition-of-template-specialization-when-using-different-objects
// for explanation of inline
template<>
inline BinaryTree<string>& BinaryTree<string>::insert<string>(const string& val)
{
    if(!m_caseSensitive)
    {
        string str = val;
        transform(val.begin(), val.end(), str.begin(), ::tolower);
        return _insert(str, m_root);
    }

    return _insert(val, m_root);
}

// insert vector, array
template<class T>
template<template<class...> class C>
BinaryTree<T>& BinaryTree<T>::insert(const C<T>& container)
{
    for(const auto& elem : container)
        insert(elem);
    return *this;
}

template<class T>
weak_ptr<Node<T>> BinaryTree<T>::search(const T& val)
{
    return (!m_root) ? weak_ptr<Node<T>>() : _search(val, m_root);
}

template<class T>
shared_ptr<Node<T>>& BinaryTree<T>::_search(const T& val, shared_ptr<Node<T>>& startNode)
{
    if(!startNode || val == startNode->value()) // either reached the end (nullptr) or found the value
        return startNode;
    else if(val < startNode->value()) // keep searching to the left
        return _search(val, startNode->left());
    else // keep searching to the right
        return _search(val, startNode->right());
}

template<class T>
weak_ptr<Node<T>> BinaryTree<T>::findMax()
{
    return (!m_root) ? weak_ptr<Node<T>>() : _findMax(m_root);
}

template<class T>
shared_ptr<Node<T>>& BinaryTree<T>::_findMax(shared_ptr<Node<T>>& startNode)
{
    if(startNode->right()) // keep going to the right
        return _findMax(startNode->right());
    return startNode; // no further nodes to the right => node is the maximum.
}

template<class T>
weak_ptr<Node<T>> BinaryTree<T>::findMin()
{
    return (!m_root) ? weak_ptr<Node<T>>() : _findMin(m_root);
}

template<class T>
shared_ptr<Node<T>>& BinaryTree<T>::_findMin(shared_ptr<Node<T>>& startNode)
{
    if(startNode->left()) // keep going to the left
        return _findMin(startNode->left());
    return startNode; // no further nodes to the left => node is the minimum.
}

template<class T>
void BinaryTree<T>::_connectNodes(shared_ptr<Node<T>>& child, shared_ptr<Node<T>> parent)
{
    // 1. parent -> child link
    if(!parent)
        m_root = child;
    else
        parent->setChild(child);

    // 2. child -> parent link (parent can be nullptr)
    child->setParent(parent);
}

template<class T>
void BinaryTree<T>::_replaceNode(shared_ptr<Node<T>>& oldNode, shared_ptr<Node<T>> newNode)
{
    // ATTN: newNode must have either 0 or 1 child for this to work. This is a private fn that
    // facilitates node removal and is not meant to be used from the outside

    // 1. make downstream connection from oldNode's parent to newNode
    if(!oldNode->parent().lock())
        m_root = newNode;
    else
        oldNode->parent().lock()->setChild(newNode);

    // 2. make bidirectional connection between newNode's only child (if any) and newNode's (current) parent
    if(newNode->parent().lock() != oldNode)
    {
        if(newNode->left())
            _connectNodes(newNode->left(), newNode->parent().lock());
        else if(newNode->right())
            _connectNodes(newNode->right(), newNode->parent().lock());
        else // newNode has no children, so instead we need to break the connection FROM its parent
            newNode->unparent(Direction::from);
    }

    // 3. make upstream connection from newNode to oldNode's parent
    if(oldNode->parent().lock())
        newNode->setParent(oldNode->parent().lock());
    else
        newNode->setParent(nullptr);

    // 4. make bidirectional connection between children of oldNode and newNode
    if(oldNode->left() != newNode)
        _connectNodes(oldNode->left(), newNode);
    if(oldNode->right() != newNode)
        _connectNodes(oldNode->right(), newNode);
}

template<class T>
BinaryTree<T>& BinaryTree<T>::remove(const T& val)
{
    auto nodeToRemove = _search(val, m_root);

    if(nodeToRemove)
    {
        // no children
        if(!nodeToRemove->left() && !nodeToRemove->right())
        {
            nodeToRemove->unparent(Direction::from); // just remove the parent's link to the node
            if(nodeToRemove == m_root)
                m_root = nullptr; // also set the root in case we removed it
        }
        // two children (replace with largest node on the left of the node)
        else if(nodeToRemove->left() && nodeToRemove->right())
            _replaceNode(nodeToRemove, _findMax(nodeToRemove->left())->shared_from_this()); // TODO: add option for smallest on the right
        // one left child
        else if(nodeToRemove->left())
            _connectNodes(nodeToRemove->left(), nodeToRemove->parent().lock());
        // one right child
        else if(nodeToRemove->right())
            _connectNodes(nodeToRemove->right(), nodeToRemove->parent().lock());

        m_numNodes--;
    }
    else if(m_verbose)
    {
        cout << "Could not find the provided value (" << val <<"). No removal action performed." << endl;
    }

    return *this;
}

template<class T>
void BinaryTree<T>::pprint() const
{
    if(!m_root)
        cout << "Empty tree." << endl;
    else
    {
        map<int, vector<T>> coords;
        _pprintNodes(coords, m_root);
        printSTLMapOfVectors(coords);
    }
}

template<class T>
void BinaryTree<T>::_pprintNodes(map<int, vector<T>>& coords, const shared_ptr<Node<T>>& node, const int& level) const
{
    if(node->left())
        _pprintNodes(coords, node->left(), level + 1);

    coords[level].push_back(node->value());

    if(node->right())
        _pprintNodes(coords, node->right(), level + 1);
}
