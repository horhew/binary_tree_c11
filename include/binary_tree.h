#ifndef BINARY_TREE_H
#define BINARY_TREE_H

#include <functional>
#include <queue>

#include "node.h"

using namespace std;

template<class T>
class BinaryTree
{
private:
    // values
    bool                m_caseSensitive = true;
    int                 m_numNodes = 0;
    shared_ptr<Node<T>> m_root;
    TraversalMethod     m_traversalMethod = TraversalMethod::inorder_asc;
    bool                m_verbose = true;

    // methods
    void                 _connectNodes(shared_ptr<Node<T>>& child, shared_ptr<Node<T>> parent);
    void                 _copyNode(shared_ptr<Node<T>>& otherNode, shared_ptr<Node<T>>& newNode);
    void                 _destroyNode(shared_ptr<Node<T>>& node,const TraversalMethod& method);
    shared_ptr<Node<T>>& _findMin(shared_ptr<Node<T>>& startNode);
    shared_ptr<Node<T>>& _findMax(shared_ptr<Node<T>>& startNode);
    BinaryTree<T>&       _insert(const T& val, shared_ptr<Node<T>>& startNode);
    void _pprintNodes(map<int, vector<T>>& coords,const shared_ptr<Node<T>>& node, const int& level = 1) const;
    void _printNode(const shared_ptr<Node<T>>& node, const TraversalMethod& method);
    void _printNodeToVec(const shared_ptr<Node<T>>& node, const TraversalMethod& method, vector<T>& out);
    void _replaceNode(shared_ptr<Node<T>>& oldNode, shared_ptr<Node<T>> newNode);
    shared_ptr<Node<T>>& _search(const T& val, shared_ptr<Node<T>>& startNode);
    template<class... Args1, class... Args2>
    void traverse(void (BinaryTree<T>::*fn)(Args1...), // function pointer
                  shared_ptr<Node<T>>& node, // node to start traversing from
                  const TraversalMethod& method = TraversalMethod::none, // traversal method
                  Args2&... args); // other args - ATTN!: as references (e.g. output vector)

public:
    // The BinaryTree constructor itself is not a template. It just takes a T as an argument.
    BinaryTree() {}
    BinaryTree(BinaryTree& other);
    BinaryTree(const T& val) { insert(val); }

    // TODO: subtree copy ctor, subtree copy assignment operator
    // TODO: cout friend
    // TODO: Balanced tree algorithms.

    void                destroy();
    void                destroy(shared_ptr<Node<T>>& startNode);
    template<template<class...> class C>
    BinaryTree<T>&      insert(const C<T>& container); // insert by container (e.g. vector)
    BinaryTree<T>&      insert(const T& val); // insert by single value
    weak_ptr<Node<T>>   findMax();
    weak_ptr<Node<T>>   findMin();
    BinaryTree<T>&      operator=(BinaryTree& other); // copy assignment operator
    BinaryTree<T>&      remove(const T& val);
    weak_ptr<Node<T>>   search(const T& val);
    void                setCaseSensitive(bool cs) { m_caseSensitive = cs; }
    void                setTraversalMethod(const TraversalMethod& method) { m_traversalMethod = method; }

    // utils
    bool isCaseSensitive() const { return m_caseSensitive; }
    int  numNodes() const { return m_numNodes; }
    void print(const TraversalMethod& method = TraversalMethod::none);
    void printToVec(vector<T>& out, const TraversalMethod& method = TraversalMethod::none);
    void pprint() const;
    weak_ptr<Node<T>> root() { return m_root; }
    const TraversalMethod& traversalMethod() const { return m_traversalMethod; }
};

#include "binary_tree.inl"

#endif // BINARY_TREE_H
